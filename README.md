#Workload Sheet Creator v1.0

    NOTE: This branch is for documentation purposes
    Please find the code to this project in the corresponding branch

##What It Is

    The Workload Sheet Creator is a tool used to create a workload sheet for academic staff
    They fill out their details and from there the workload sheet is created

##Features

    Create PDF from filled in information

###How to use

    1. Open https://workloadsheet.000webhostapp.com
    2. Fill out needed information
    3. Click Save
    4. Your load sheet has been saved

###Future Development Plans
    1. Automate the process further
    2. Distribute workload percentages
    3. Better UI design
    4. And more 
